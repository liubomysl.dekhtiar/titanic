import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    mr = []
    mrs = []
    miss = []
    nulls = df['Age'].isnull().values
    x = 0
    k = 0
    l = 0
    for i, value in enumerate(df['Name']):
        name = value.split(', ')[1]
        title = name.split(' ')[0]

        if nulls[i]:
            if title == 'Mr.':
                x += 1
            elif title == 'Mrs.':
                k += 1
            elif title == 'Miss.':
                l += 1
            continue

        if title == 'Mr.':
            mr.append(df.iloc[i]['Age'])
        elif title == 'Mrs.':
            mrs.append(df.iloc[i]['Age'])
        elif title == 'Miss.':
            miss.append(df.iloc[i]['Age'])

    mr = np.array(mr)
    mrs = np.array(mrs)
    miss = np.array(miss)

    y = round(np.median(mr))

    m = round(np.median(mrs))

    n = round(np.median(miss))
    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
